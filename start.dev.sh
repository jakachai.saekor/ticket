#!/bin/bash

source ./.env.dev

network_name=$(echo $PROJECT_NAME)
network_id=$(docker network list | grep $network_name | awk '{ print $1 }')

if [ ! $network_id ]; then
    echo "Docker network '$network_name' does not exists"
    echo "Creating docker network '$network_name'"

    docker network create $network_name
else
    echo "Docker network '$network_name' exists"
    echo "Skip creating docker network '$network_name'"
fi

cp .env.dev api/compose/.env
cd api/compose
docker-compose \
    -f docker-compose.yaml \
    -f docker-compose.dev.yaml \
    up -d \
    --force-r --build
cd ../..

cp .env.dev cicd/compose/.env
cd cicd/compose
docker-compose up -d --force-r --build
cd ../..

cp .env.dev monitoring/compose/.env
cd monitoring/compose
docker-compose up -d --force-r --build
cd ../..

docker ps --filter "label=project.name=$network_name"
# docker ps -a --filter "label=project.name=ticket"