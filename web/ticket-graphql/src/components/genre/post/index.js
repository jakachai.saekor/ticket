import React from 'react'
import { Link } from 'react-router-dom'

export default ({ post }) => (
  <Link
    className="bg-white ma3 box post flex flex-column no-underline br2"
    to={`/post/${post.id}`}
  >
    <div
      className="image"
      style={{
        backgroundImage: `url(${post.imageUrl})`,
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        paddingBottom: '100%',
      }}
    />
    <div className="flex items-center black-80 fw3 description">
      {post.description}
    </div>
  </Link>
)
