import React from 'react'
import { ApolloProvider } from 'react-apollo'
import { hot } from 'react-hot-loader'

import 'tachyons'
import './index.css'

import { client } from './apollo'
import { Index } from './pages'

const App = () => (
  <ApolloProvider client={client}>
    <Index />
  </ApolloProvider>
)

export default hot(module)(App)
