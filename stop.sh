#!/bin/bash

source .env

network_name=$(echo $PROJECT_NAME)

cp .env api/compose
cd api/compose
docker-compose down
cd ../..

cp .env cicd/compose
cd cicd/compose
docker-compose down
cd ../..

cp .env monitoring/compose
cd monitoring/compose
docker-compose down
cd ../..

docker ps -a --filter "label=project.name=$network_name"