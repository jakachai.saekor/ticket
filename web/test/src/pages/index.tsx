import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Icon } from 'antd'

import Layout from '../layout'
import { load } from '../reduxs/hello-world/action'
import { IAppState } from '../common/state'

// interface IProps extends IHelloWorldState {
//   dispatch: Function
// }

class Index extends Component<{}, {}> {
  componentDidMount() {
    const { dispatch }: any = this.props
    dispatch(load())
  }

  render() {
    const { message }: any = this.props
    return (
      <Layout>
        {/* {loading && <Icon type="loading-3-quarters" theme="outlined" />} */}
        <div className="flex">
          <div className="mr-2">
            <Icon type="loading" theme="outlined" spin />
          </div>

          <div id="message">{message}</div>
        </div>
      </Layout>
    )
  }
}

export default connect(
  (state: IAppState) => state.helloWorld,
  dispatch => ({ dispatch }),
)(Index)
