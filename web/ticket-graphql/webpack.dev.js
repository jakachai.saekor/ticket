const { DefinePlugin, HotModuleReplacementPlugin } = require('webpack')
const merge = require('webpack-merge')

const common = require('./webpack.common')

module.exports = merge(common, {
  mode: 'development',
  devtool: 'inline-source-map',
  devServer: {
    hot: true,
    inline: true,
    host: '0.0.0.0',
    contentBase: './',
    publicPath: '/',
    historyApiFallback: true,
  },
  plugins: [
    new DefinePlugin({
      'process.env': {
        SIMPLE_URI: JSON.stringify(
          'http://localhost:60000/simple/v1/cjsac2m0s00040189ev1znfkh',
        ),
      },
    }),

    // remove --hot from webpack-dev-server command
    new HotModuleReplacementPlugin(),
  ],
})
