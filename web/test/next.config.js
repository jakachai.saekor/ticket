console.log('process.env.NODE_ENV', process.env.NODE_ENV)

const { parsed: localEnv } = require('dotenv').config({
  path: `./env/${process.env.NODE_ENV}`,
})
const withTypescript = require('@zeit/next-typescript')
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin')

console.log('localEnv', localEnv)

module.exports = withTypescript({
  publicRuntimeConfig: {
    ...localEnv,
  },
  webpack: (config, options) => {
    config.module.rules.push({
      test: /\.css$/,
      use: ['babel-loader', 'raw-loader', 'postcss-loader'],
    })

    if (options.isServer) {
      config.plugins.push(new ForkTsCheckerWebpackPlugin())
    }

    return config
  },
  webpackDevMiddleware: config => config,
})
