import { connect } from './connector'
import movie from '../models/movie'

export default {
  movies: async () => {
    console.log(1)
    let mongoose
    let movies

    try {
      mongoose = connect()
      movies = await movie()
        //
        .find()
        .select({
          id: 1,
          name: 1,
          genre: 1,
          rate: 1,
          length: 1,
          language: 1,
        })
        .exec()

      console.log('movies', movies)

      return movies
    } catch (error) {
      throw error
    } finally {
      mongoose.disconnect()
    }

    return movies

    // return [
    //   {
    //     id: 1,
    //     name: 'movie001',
    //     genre: 'fantasy',
    //     rate: 'age13',
    //     length: 120,
    //     language: 'EN',
    //   },
    //   {
    //     id: 2,
    //     name: 'movie002',
    //     genre: 'action',
    //     rate: 'age18',
    //     length: 160,
    //     language: 'EN',
    //   },
    // ]
  },
}
