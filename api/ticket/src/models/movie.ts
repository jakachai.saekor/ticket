import mongoose from 'mongoose'
import sequence from 'mongoose-sequence'

const AutoIncrement = sequence(mongoose)
let schema

export default function Movie() {
  if (!schema) {
    schema = createSchema()
  }

  return mongoose.model('Movie', schema, 'Movies')
}

const createSchema = () => {
  const Schema = mongoose.Schema
  const _schema = new Schema(
    {
      name: {
        //
        type: String,
        required: true,
        unique: true,
      },

      genre: { type: String },
      rate: { type: String },
      length: { type: Number },
      language: { type: String },
    },
    { timestamps: true },
  )

  _schema.plugin(AutoIncrement, { inc_field: 'id' })
  return _schema
}
