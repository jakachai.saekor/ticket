import React from 'react'

const Layout = ({ children }) => (
  <div className="app">
    <div>Header</div>
    <div>
      <div>Sidebar</div>
      <main className="main">
        {/* <Breadcrumb /> */}
        {/* <Container fluid>{children}</Container> */}
        {children}
      </main>
    </div>
    <div>Footer</div>
  </div>
)

export default Layout
