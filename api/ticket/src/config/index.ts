export const port: number =
  (process.env.PORT && Number.parseInt(process.env.PORT, 10)) || 3000

export interface DbConfig {
  host: string
  port: number
  username: string
  password?: string
  database: string
}

export const db: DbConfig = {
  host: process.env.MONGO_HOST || '127.0.0.1',
  port: Number(process.env.MONGO_POST) || 27017,
  username: process.env.MONGO_USERNAME || 'ticket',
  password: process.env.MONGO_PASSWORD || 'ticket',
  database: process.env.MONGO_DATABASE || 'ticket',
}
