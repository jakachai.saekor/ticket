const { DefinePlugin } = require('webpack')
const merge = require('webpack-merge')
const common = require('./webpack.common.js')

module.exports = merge(common, {
  mode: 'production',
  plugins: [
    new DefinePlugin({
      'process.env': {
        //
        PROTOCAL: JSON.stringify('https'),
        HOST: JSON.stringify(''),
        PORT: JSON.stringify(''),
        PATH: JSON.stringify('/api/v1'),
        //
        ETL_PROTOCAL: JSON.stringify('https'),
        ETL_HOST: JSON.stringify(''),
        ETL_PORT: JSON.stringify(''),
        ETL_PATH: JSON.stringify('/etl/api/v1'),
        //
        BASE_PATH: JSON.stringify('/operation/v2'),
        //
        APIKEY: JSON.stringify(
          'wkrR38WzocgToztFsvOD9k2OBaBWA5HsLekRPe2EbuxCnjp42lI2Shqj1caLGT50',
        ),
      },
    }),
  ],
})
