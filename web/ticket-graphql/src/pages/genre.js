import React from 'react'

import { List } from '../components/genre/list'
import { Create } from '../components/genre/create'
import { View } from '../components/genre/view'

export default () => (
  <>
    <Create />
    <View />
    <List />
  </>
)
