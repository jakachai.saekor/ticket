import { of, from } from 'rxjs'
import { Epic } from 'redux-observable'
import { switchMap, map, catchError, takeUntil } from 'rxjs/operators'
// import router from 'next/router'

import { LOAD, CANCEL, done } from './action'

export const loadEpic: Epic = (action$, {}, { url, getJson }) =>
  action$
    //
    .ofType(LOAD)
    //
    .pipe(
      //
      switchMap(() =>
        from(getJson(url('/helloworld')))
          //
          .pipe(
            //
            map(({ data }: any) => done(data)),

            takeUntil(action$.ofType(CANCEL)),

            catchError(_error => {
              return of(done({ error: _error.message }))
            }),
          ),
      ),
    )
