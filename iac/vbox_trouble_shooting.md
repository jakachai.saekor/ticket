# Issue: Cannot unregister the machine 'vagrant_node-02_1550314808328_43453' while it is locked

## Solution:

- Find running process of virtualbox

```
    ps aux | grep -i virtualbox
```

- Delete running process of virtualbox

```
    kill -9 <process-id>
```
