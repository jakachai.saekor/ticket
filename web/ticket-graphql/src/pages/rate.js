import React from 'react'

import { List } from '../components/rate/list'
import { Create } from '../components/rate/create'
import { View } from '../components/rate/view'

export default () => (
  <>
    <Create />
    <View />
    <List />
  </>
)
