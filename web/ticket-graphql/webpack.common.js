const path = require('path')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')

const { version } = require('./package.json')
const { resolve } = path

module.exports = {
  cache: true,
  entry: {
    main: './src/index.js',
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        use: ['babel-loader', 'eslint-loader'],
        exclude: /(node_modules|bower_components)/,
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.scss$/,
        use: ['style-loader', 'css-loader', 'sass-loader'],
      },
      {
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: 'url-loader?limit=10000&mimetype=application/font-woff',
      },
      {
        test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: 'file-loader',
      },
      {
        test: /\.(png|jpg|jpeg|gif|ico)$/,
        use: 'file-loader',
      },
    ],
  },
  // resolve: {
  //   extensions: ['.js', '.jsx', '.json'],
  //   alias: {
  //     redux: path.resolve(__dirname, 'src/redux/'),
  //     // Templates: path.resolve(__dirname, 'src/templates/'),
  //   },
  // },
  plugins: [
    new CleanWebpackPlugin(['dist']),
    new HtmlWebpackPlugin({
      title: `e-LG on Blockchain for Operation v${version}`,
      template: resolve(__dirname, 'public', 'index.html'),
      favicon: './public/favicon.png',
      minify: { collapseWhitespace: true },
    }),
  ],
  output: {
    filename: '[name].bundle.js',
    chunkFilename: '[name].bundle.js',
    path: resolve(__dirname, 'dist'),
  },
}
