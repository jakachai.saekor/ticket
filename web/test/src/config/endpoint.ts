import getConfig from 'next/config'

export function config() {
  const { publicRuntimeConfig } = getConfig()
  const { HOST, PORT, PATH } = publicRuntimeConfig
  const host = HOST || window.location.hostname
  const port = PORT
  const _port = (port && `:${port}`) || ''
  const path = PATH || ''

  return { host, port: _port, path }
}

export const apiUrl = (apiPath: string = ''): string => {
  const { host, port, path } = config()
  return `http://${host}${port}${path}${apiPath}`
}

export const wsUrl = (): string => {
  const { host, port } = config()
  return `http://${host}${port}` || 'http://localhost:5000'
}
