import { createStore, applyMiddleware, compose } from 'redux'
import { createEpicMiddleware } from 'redux-observable'

import { apiUrl } from '../config/endpoint'
import { getJson, postJson } from '../common/request'
import rootReducer from './reducer'
import rootEpic from './epic'

export default () => {
  const epicMiddleware = createEpicMiddleware({
    dependencies: { url: apiUrl, getJson, postJson },
  })
  const composeEnhancers = compose
  // window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

  const store = createStore(
    rootReducer,
    composeEnhancers(applyMiddleware(epicMiddleware)),
  )

  epicMiddleware.run(rootEpic)

  return store
}
