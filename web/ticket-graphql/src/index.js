import React from 'react'
import { render } from 'react-dom'

import App from './app'

render(<App />, document.querySelector('react'))

if (module.hot) {
  module.hot.accept('./app', () => render(<App />))
}
