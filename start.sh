#!/bin/bash

source .env

network_name=$(echo $PROJECT_NAME)
network_id=$(docker network list | grep $network_name | awk '{ print $1 }')

if [ ! $network_id ]; then
    echo "Docker network '$network_name' does not exists"
    echo "Creating docker network '$network_name'"

    docker network create $network_name
else
    echo "Docker network '$network_name' exists"
    echo "Skip creating docker network '$network_name'"
fi



# it doesn't work
# cp .env db/compose
# cp .env cicd/compose
# cp .env monitoring/compose

# docker-compose \
#     -f db/compose/docker-compose.yaml \
#     -f cicd/compose/docker-compose.yaml \
#     -f monitoring/compose/docker-compose.yaml \
#     up -d --force-r --build



cp .env db/compose
cd db/compose
docker-compose up -d --force-r --build
cd ../..

# sleep 2

cp .env.dev api/compose/.env
cd api/compose
docker-compose up -d --force-r --build
cd ../..

# sleep 2

cp .env cicd/compose
cd cicd/compose
docker-compose up -d --force-r --build
cd ../..

# sleep 2

cp .env monitoring/compose
cd monitoring/compose
docker-compose up -d --force-r --build
cd ../..

docker ps -a --filter "label=project.name=$network_name"
# docker ps -a --filter "label=project.name=ticket"