#!/bin/bash

# VBoxManage list vms
# VBoxManage list vms -l
# VBoxManage unregistervm node-01
# vboxmanage unregistervm node-01 --delete
# vboxmanage controlvm node-01 poweroff 

# for f in $(VBoxManage list runningvms | awk -F\" '{print $2}'); do
#     echo "$f:"
#     VBoxManage guestproperty enumerate "$f" | grep IP
# done