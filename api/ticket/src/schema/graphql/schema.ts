import { buildSchema } from 'graphql'

export default buildSchema(`
  type Query {
    movies(name: String, genre: Genre, rate: Rate, language: String): [Movie]
  }

  type Movie {
    id: Int
    name: String
    genre: Genre
    rate: Rate
    length: Float
    language: String
  }

  enum Genre {
    fantasy
    documentary
    action
    horror
    animation
    thriller
    drama
    biography
    musical
  }

  enum Rate {
    G
    TBC
    age18
    age15
    age13
  }
`)
