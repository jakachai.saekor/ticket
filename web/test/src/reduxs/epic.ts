import { combineEpics } from 'redux-observable'
import { loadEpic } from './hello-world/epic'

export default combineEpics(loadEpic)
