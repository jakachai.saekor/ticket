import React from 'react'

import { List } from '../components/movie/list'
import { Create } from '../components/movie/create'
import { View } from '../components/movie/view'

export default () => (
  <>
    <Create />
    <View />
    <List />
  </>
)
