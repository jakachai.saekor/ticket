resource "virtualbox_vm" "node" {
  count = 1
  name  = "${format("node-%02d", count.index+1)}"

  # url    = "https://app.vagrantup.com/ubuntu/boxes/xenial64/versions/20190215.0.0/providers/virtualbox.box"
  image = "./virtualbox.box"

  # cpus   = 1
  memory = "256 mib"

  // user_data = "${file("user_data")}"

  network_adapter {
    type           = "hostonly"
    host_interface = "vboxnet0"
  }
}
