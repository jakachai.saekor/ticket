repoName = 'ticket.git'
dockerUrl = ''
dockerPrivateRegistry = 'j2sdkball/'
webArtifactoryUrl = 'https://artifactory.kasikornbank.com:8443/artifactory/ORIGINCERT_747/DEV/origincert/operation-app/'
composeArtifactoryUrl = 'https://artifactory.kasikornbank.com:8443/artifactory/ORIGINCERT_747/DEV/origincert/operation-compose/'
dockerImage = "${dockerUrl}node:11.4.0-alpine"
httpProxy = ''
httpsProxy = ''
noProxy = ''
dockerArgs = """
            -u root:root \
            -e http_proxy=${httpProxy} \
            -e https_proxy=${httpsProxy} \
            -e no_proxy=${noProxy}
            """
dockerfileOption = '-f dockerfile.kbank'
apiDir = 'api/ticket/'
webDir = 'web/test/'
ansibleDir = '.ansible/'
version = ''
checksum = ''
workspace = ''
packPath = ''
yarnInstall = 'npm i -g yarn@1.13.0'

pipeline {
    agent {
		label 'master'
    }

    // must call deleteDir() and checkout scm if skipDefaultCheckout = true
    options {
        skipDefaultCheckout true
    }

    stages {
        stage('Lint Check') {
            agent {
                docker {
                    image "${dockerImage}"
                    args  "${dockerArgs}"
                    reuseNode true
                }
            }
            steps {
                // deleteDir()
                checkout scm

                script {
                    workspace = WORKSPACE
                }
                
                echo "workspace: ${workspace}"

                sh 'rm -rf tmp'
                sh 'mkdir tmp'
                sh 'chmod -R 777 tmp'

                //   sh 'npm set strict-ssl false'
                //   sh 'yarn config set strict-ssl false'
                sh "${yarnInstall}"
                sh "cd ${apiDir} && yarn && yarn lint"
                sh "cd ${webDir} && yarn && yarn lint"
            }
        }

        stage('Unit Test') {
            agent {
                docker {
                    image "${dockerImage}"
                    args  "${dockerArgs}"
                    reuseNode true
                }
            }
            steps {
                // sh "${yarnInstall}"
                sh "cd ${apiDir} && yarn && npx mocha --require ts-node/register **/**/*.spec.ts"
                sh "cd ${webDir} && yarn && yarn test"
            }
        }

        stage('Dependency Check') {
            agent {
                docker {
                    image "${dockerImage}"
                    args  "${dockerArgs}"
                    reuseNode true
                }
            }
            steps {
                // sh "${yarnInstall}"
                        
                script {
                    sh(script: "cd ${apiDir} && (yarn audit | grep critical) && exit 2 || exit 0", returnStdout: true)
                    sh(script: "cd ${webDir} && (yarn audit | grep critical) && exit 2 || exit 0", returnStdout: true)
                }
            }
        }

        stage('Code Analysis') {
            environment {
                // scannerHome: /var/jenkins_home/tools/hudson.plugins.sonar.SonarRunnerInstallation/SonarQubeScanner
                scannerHome = tool 'SonarQubeScanner'
            }
            steps {
                sh 'node --version'
                sh 'npm --version'

                withSonarQubeEnv('sonarqube') {
                    dir('web') {
                        sh """
                            ${scannerHome}/bin/sonar-scanner \
                            -Dsonar.projectKey=ticket.api \
                            -Dsonar.projectName=ticket.api \
                            -Dsonar.projectVersion=0.1 \
                            -Dsonar.projectDescription='API for Ticket' \
                            -Dsonar.sources=./src \
                            -Dsonar.language=ts \
                            -Dsonar.profile='Sonar way' \
                            -Dsonar.dynamicAnalysis=reuseReports \
                            -Dsonar.sourceEncoding=UTF-8 \
                            -Dsonar.exclusions=.jenkins/**,dist/**,env/**,node_modules/**/*
                        """
                    }
                }
            }
        }

        stage('Quality Gate') {
            steps {
                timeout(time: 10, unit: 'MINUTES') {
                    waitForQualityGate abortPipeline: true
                }   
            }
        }

        // stage('Build API Artifact') {
        //     steps {
        //         dir("${apiDir}") {
        //             sh "docker build -t ticket:${version} ."
        //             sh "docker tag ticket:${version} ${dockerPrivateRegistry}ticket:${version}"
        //         }
        //     }
        // }

        // stage('Push API Artifact') {
        //     steps {
        //         sh "docker login -u ${DOCKER_USERNAME} -p ${DOCKER_PASSWORD}"
        //         sh "docker push ${dockerPrivateRegistry}ticket:${version}"
        //     }
        // }

        // stage('Build Web Artifact') {
        //     agent {
        //         docker {
        //             image "${dockerImage}"
        //             args  "${dockerArgs}"
        //             reuseNode true
        //         }
        //     }
        //     steps {
        //         sh 'npm set strict-ssl false'
        //         sh 'yarn config set strict-ssl false'
        //         sh "${yarnInstall}"
        //         // sh 'npm i -g webpack webpack-cli'
        //         // sh "apk add --no-cache curl"

        //         sh "cd ${webDir} && yarn && yarn build"

        //         sh """cd ${webDir} && tar -pcvzf operation-app.${version}.tar.gz dist"""
        //         sh """cd ${webDir} && cp operation-app.${version}.tar.gz ${workspace}/tmp"""

        //         sh "chmod -R 777 ${workspace}/tmp"
        //         sh "ls -la ${workspace}/tmp"
        //     }
        // }

        // stage('Push Web Artifact') {
        //     steps {
        //         sh "ls -la ${workspace}/tmp"

        //         sh """curl -u origincert:[vdw,j5^d \
        //             -T ${workspace}/tmp/operation-app.${version}.tar.gz \
        //             '${webArtifactoryUrl}operation-app.${version}.tar.gz' -k"""

        //         dir("${workspace}/tmp") {
        //             sh "rm -rf operation-app.${version}.tar.gz"
        //         } 
        //     }
        // }

        // stage('Generate Checksum') {
        //     steps {
        //         script {
        //             checksum = sh(script: "tar -cf - api/ | md5sum | tr -d ' '| sed 's/-//' | tr -d '\n'", returnStdout: true)
                    
        //             echo "checksum: ${checksum}"
        //         }
        //     }
        // }

        // stage('Build Api Artifact') {
        //     steps {
        //         script {
        //             checksum = sh(script: "tar -cf - api/ | md5sum | tr -d ' '| sed 's/-//' | tr -d '\n'", returnStdout: true)
                    
        //             echo "checksum: ${checksum}"
        //         }

        //         dir("${apiDir}") {
        //             sh "docker build ${dockerfileOption} -t operation-api:${version} ."
        //             sh "docker tag operation-api:${version} ${dockerUrl}origincert/operation-api:${version}-${checksum}"
        //         }
        //     }
        // }

        // stage('Push Api Artifact') {
        //     steps {
        //         sh "docker push ${dockerUrl}origincert/operation-api:${version}-${checksum}"
        //     }
        // }

        // stage('Build Compose Artifact') {
        //     steps {

        //         script {
        //             packPath = "package/operation-compose.${version}"
        //         }

        //         sh "rm -rf ${packPath}"
        //         sh "mkdir -p ${packPath}"
        //         sh "chmod -R 777 ${packPath}"

        //         sh "cp start*.sh ${packPath}"
        //         sh "cp stop*.sh ${packPath}"
        //         sh "cp .env* ${packPath}"
        //         sh "cp README.md ${packPath}"

        //         sh "cp -r db ${packPath}"
        //         sh "cp -r api ${packPath}"
        //         sh "cp -r .nginx ${packPath}"
        //         sh "rm -rf ${packPath}/api/etl"

        //         dir("package/") {
        //             sh "tar -pcvzf operation-compose.${version}.tar.gz operation-compose.${version}"
        //         }
        //     }
        // }

        // stage('Push Compose Artifact') {
        //     steps {
        //         dir("package/") {
        //             sh "ls -la ."

        //             sh """curl -u origincert:[vdw,j5^d \
        //                 -T operation-compose.${version}.tar.gz \
        //                 '${composeArtifactoryUrl}operation-compose.${version}.tar.gz' -k"""

        //             dir("package/") {
        //                 sh "rm -rf operation-compose.${version}"
        //                 sh "rm -rf operation-compose.${version}.tar.gz"
        //             } 
        //         }
        //     }
        // }

        // stage('Deploy') {
        //     steps{
        //         dir(".ansible/") {
        //             sh """echo -e "env: dev\nversion: ${version}\nchecksum: ${checksum}\n" > ./inventory/kbtg/group_vars/all.yaml"""
        //             sh "cat ./inventory/kbtg/group_vars/all.yaml"
        //         }
        //     }
        // }

        // stage('Deploy Web') {
        //     agent {
        //         label "${DEPLOY_SLAVE}"
        //     }
        //     steps{
        //         dir("${WEB_PATH}") {
                    // sh """curl -o operation-app.${version}.tar.gz \ 
                    //     -u origincert:[vdw,j5^d \
                    //     '${webArtifactoryUrl}operation-app.${version}.tar.gz' -k"""

        //             sh "tar -xzf operation-app.${version}.tar.gz"
        //             sh 'cp dist/* .'
        //             sh 'rm -r dist'
        //             sh "rm operation-app.${version}.tar.gz"
        //             sh 'docker restart nginx'
        //         }
        //     }
        // }

        // stage('Deploy Api') {
        //     agent {
        // 		label "${DEPLOY_SLAVE}"
        //     }
        //     steps {
        //         sh """docker pull ${dockerUrl}origincert/operation-api:${version}-${checksum}"""
        //         sh """docker tag ${dockerUrl}origincert/operation-api:${version}-${checksum} operation-api:latest"""
                
        //         // dir("${COMPOSE_PATH}") {
        //         //     sh "docker-compose -f temp2.yml up --force-recreate -d"
        //         // }
        //     }
        // }
    }
}