import express, { Application, Request, Response } from 'express'
import cors from 'cors'
import bodyParser from 'body-parser'
import expressGraphql from 'express-graphql'

import { init as initLog, logger } from './common/log'
import { name, version, description, author } from '../package.json'
import { port, DbConfig } from './config'
import schema from './schema/graphql/schema'
import resolver from './data/resolver'
import { db } from './config'

const app: Application = express()

app.use(cors())
app.use(bodyParser.json())

app.use(
  '/graphql',

  expressGraphql({
    schema: schema,
    rootValue: resolver,
    graphiql: true,
  }),
)

app.get('/', (req: Request, res: Response) =>
  res.json({ name, version, description, author }),
)

initLog()

app.listen(port, () => {
  logger.info(`server started at http://localhost:${port}`)

  const { host, username, database } = db
  const config: DbConfig = { host, port: db.port, username, database }

  logger.info(`database config: ${JSON.stringify(config)}`)
})
