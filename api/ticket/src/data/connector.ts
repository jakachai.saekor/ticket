import Mongoose from 'mongoose'
import bluebird from 'bluebird'

import { db } from '../config'
import { logger } from '../common/log'

Mongoose.Promise = bluebird

const { host, port, username, password, database } = db

export const connect = () => {
  Mongoose.connect(
    `mongodb://${username}:${password}@${host}:${port}/${database}`,

    {
      useCreateIndex: true,
      useNewUrlParser: true,
    },
  ).then(
    //
    () => logger.info('mongo connected'),
    error => logger.info('mongo connection error', error),
  )
  return Mongoose
}
