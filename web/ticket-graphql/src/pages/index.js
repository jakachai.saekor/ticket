import React, { Suspense, lazy } from 'react'
import { HashRouter, Route, Switch } from 'react-router-dom'
import { Spin, Icon } from 'antd'

const Layout = lazy(() => import('../layouts'))
const Genre = lazy(() => import('./genre'))
const Rate = lazy(() => import('./rate'))
const Movie = lazy(() => import('./movie'))

const icon = <Icon type="loading" style={{ fontSize: 24 }} spin />

export const Index = () => (
  <div>
    <HashRouter>
      <Suspense fallback={<Spin indicator={icon} />}>
        <Switch>
          {/*  */}
          <Route
            exact
            path="/movie"
            render={() => (
              <Layout>
                <Movie />
              </Layout>
            )}
          />

          <Route
            path="/rate"
            render={() => (
              <Layout>
                <Rate />
              </Layout>
            )}
          />

          <Route
            path="/genre"
            render={() => (
              <Layout>
                <Genre />
              </Layout>
            )}
          />
        </Switch>
      </Suspense>
    </HashRouter>
  </div>
)
