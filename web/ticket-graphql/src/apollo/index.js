import { ApolloClient } from 'apollo-client'
import { HttpLink } from 'apollo-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory'
import fetch from 'node-fetch'

export const client = new ApolloClient({
  link: new HttpLink({
    fetch,
    uri: 'http://localhost:60000/simple/v1/cjsac2m0s00040189ev1znfkh',
  }),
  cache: new InMemoryCache(),
})
