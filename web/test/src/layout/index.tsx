import React, { ReactNode, SFC } from 'react'
import { connect } from 'react-redux'

import { IAppState } from '../common/state'

const { version } = require('../../package.json')

export interface ILayoutProps {
  children: ReactNode
}

const Index: SFC<ILayoutProps> = ({ children }) => (
  <div>
    <div className="flex flex-col font-mono">
      {/* header */}
      <header className="sticky pin-x pin-t bg-white">
        <div className="flex justify-start">
          <span className="m-4 ml-6 font-bold text-lg">
            Microservice Workshop v{version}
          </span>
        </div>
      </header>
      {/* header */}

      {/* section  */}
      <section className="flex justify-center">
        <div className="flex flex-col">{children}</div>
      </section>
      {/* section */}

      {/* footer */}
      {/* footer */}
    </div>
  </div>
)

export default connect(
  (state: IAppState) => state,
  dispatch => ({ dispatch }),
)(Index)
