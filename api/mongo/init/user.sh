#!/usr/bin/env bash

echo "Creating mongo users..."

mongo admin --host localhost -u root -p root --eval "db.getSiblingDB('ticket').createUser({user: 'ticket', pwd: 'ticket', roles: [{role: 'readWrite', db: 'ticket'}]});"

echo "Mongo users created."