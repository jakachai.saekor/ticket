// db.createUser({
//   user: 'ticket',
//   pwd: 'ticket',
//   roles: [
//     {
//       role: "readWrite",
//       db: "ticket"
//     }
//   ]
// })

[
  {
    id: 1,
    name: 'movie001',
    genre: 'fantasy',
    rate: 'age13',
    length: 120,
    language: 'EN',
  },
  {
    id: 2,
    name: 'movie002',
    genre: 'action',
    rate: 'age18',
    length: 160,
    language: 'EN',
  },
].forEach(document => db.Movies.insert(document))
