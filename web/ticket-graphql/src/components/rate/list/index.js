import React, { Component } from 'react'

import { Link, withRouter } from 'react-router-dom'
import { graphql } from 'react-apollo'
import gql from 'graphql-tag'

import Image from '../../../assets/plus.svg'

class Comp extends Component {
  componentWillReceiveProps(nextProps) {
    if (this.props.location.key !== nextProps.location.key) {
      this.props.allPostsQuery.refetch()
    }
  }

  render() {
    if (this.props.allPostsQuery.loading) {
      return (
        <div className="flex w-100 h-100 items-center justify-center pt7">
          <div>Loading (from {process.env.REACT_APP_GRAPHQL_ENDPOINT})</div>
        </div>
      )
    }

    let blurClass = ''
    if (this.props.location.pathname !== '/') {
      blurClass = ' blur'
    }

    return (
      <div className={`w-100 flex justify-center pa6${blurClass}`}>
        <div className="w-100 flex flex-wrap" style={{ maxWidth: 1150 }}>
          <Link
            to="/create"
            className="ma3 box new-post br2 flex flex-column items-center justify-center ttu fw6 f20 black-30 no-underline"
          >
            <img src={Image} alt="" className="plus mb3" />
            <div>New Post</div>
          </Link>
        </div>
        {this.props.children}
      </div>
    )
  }
}

const ALL_POSTS_QUERY = gql`
  query {
    allPosts(orderBy: createdAt_DESC) {
      id
      imageUrl
      description
    }
  }
`

export const List = withRouter(
  graphql(ALL_POSTS_QUERY, {
    name: 'allPostsQuery',
    options: {
      fetchPolicy: 'network-only',
    },
  })(Comp),
)
