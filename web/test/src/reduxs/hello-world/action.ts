export const LOAD = 'helloWorld/load'
export const load = () => ({
  type: LOAD,
  loading: true,
})

export const DONE = 'helloWorld/load/done'
export const done = (data: any = {}) => ({
  type: DONE,
  loading: false,
  ...data,
})

export const CANCEL = 'helloWorld/load/cancel'
export const cancel = (data: any = {}) => ({
  type: CANCEL,
  loading: false,
  ...data,
})

// export const ERROR = 'channel/configure/error'
// export const error = error => ({
//   type: ERROR,
//   loading: false,
//   error,
// })

// export const CANCEL = 'channel/configure/error'
// export const cancel = () => ({
//   type: CANCEL,
//   loading: false,
// })
