#!/bin/bash

go get github.com/terra-farm/terraform-provider-virtualbox
mkdir ~/.terraform.d/plugins
cp $GOPATH/bin/terraform-provider-virtualbox  ~/.terraform.d/plugins